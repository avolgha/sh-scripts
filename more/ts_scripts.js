#!/usr/bin/env node

const fs = require('fs');

const packageJson = JSON.parse(fs.readFileSync('package.json', {encoding: 'utf-8'}));

delete packageJson['scripts']['test'];
delete packageJson['keywords'];

packageJson['license'] = 'MIT';

packageJson['main'] = 'build/main.js';
packageJson['types'] = 'build/main.d.ts';

packageJson['scripts']['compile'] = 'tsc';
packageJson['scripts']['start'] = 'node .';
packageJson['scripts']['dev'] = 'tsc && node .';

fs.writeFileSync('package.json', JSON.stringify(packageJson, undefined, 4), {encoding: 'utf-8'});
