#!/usr/bin/env node

const fs = require('fs');

fs.mkdirSync('src');
fs.writeFileSync('src/main.js', 'console.log(\'Hello, World!\');');

const packageJson = require('./package.json');

packageJson['main'] = 'src/main.js';

fs.writeFileSync('package.json', JSON.stringify(packageJson, undefined, 4));
