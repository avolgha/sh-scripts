#!/usr/bin/env bash

##
## This script was created with purpose on testing, so it will not have any
## high rated features and speed improvements in it
## Feel free to edit this script and do whatever you want to do with it, except
## giving it out as YOUR content
##
## ~ avolgha (Marius), 10 Nov 2021
##

color_reset="$(tput sgr0)"
color_info="$(tput setaf 2)"
color_error="$(tput setaf 1)"
color_prompt="$(tput setaf 6)"
color_white="$(tput setaf 7)"

info() { echo "${color_info}info:${color_white}  ${1}${color_reset}"
}

error() { echo "${color_error}error:${color_white} ${1}${color_reset}"
}

fatal() {
	for arg in "$@"; do
		echo "${color_error}fatal:${color_white} ${arg} ${color_reset}"
	done
	exit 1
}

acceptFunction() {
	while true; do
		read -p "${color_prompt}prompt:${color_white} $* ${color_white}[${color_info}y${color_white}/${color_error}n${color_white}]: " yn
		case "$yn" in
			[YyJj]*) return 0 ;;
			[Nn]*) return 1 ;;
		esac
	done
}

temp_answer=""
askFunction() {
	while true; do
		read -p "${color_prompt}prompt:${color_white} ${*}${color_white}: " answer
		if ! [ "$(echo $answer | wc -w)" -eq 0 ]; then
			temp_answer=$answer; break
		fi
	done
}

update() {
	msg="$(sudo pacman -Syy)"
	ret="$?"

	if [ ! $ret -eq 0 ]; then
		fatal "${color_error}fatal: exited program with status code '$(ret)'. This might helps you to find the error:" "$msg"
	fi
}

upgrade() {
	update

	msg="$(sudo pacman -Syyu)"
	ret="$?"

	if [ ! $ret -eq 0 ]; then
		fatal "${color_error}fatal: exited program with status code '$(ret)'. This might helps you to find the error:" "$msg"
	fi
}

nodejs_exec() {
	if [ "$1" == "1" ]; then
		yarn $2 &> /dev/null
	else
		npm $2 &> /dev/null
	fi
}

generate() {
	askFunction "please provide a project template to generate (for a list of these see this flag: '--list-generates')"

	project="$temp_answer"
	if [ $project == "cpp" ]; then
		info "Setting up c++ project ..."

		dir=""
		while true; do
			askFunction "please provide a project name"
			dir="$temp_answer"
			if [ -d "$dir" ]; then
				error "This directory does already exists."
			else
				break
			fi
		done

		mkdir "$dir"; cd "$dir"

		/usr/bin/cat <<EOT >> CMakeLists.txt
project (${dir})

add_executable(${dir} src/${dir}.cpp)
EOT

		mkdir src; cd src
		/usr/bin/cat <<EOT >> "${dir}.cpp"
#include <iostream>

int main(void)
{
	std::cout << "Hello World" << std::endl;
	return 0;
}
EOT
		cd ..

		if $(acceptFunction "do you want to add a build script"); then
			/usr/bin/cat <<EOT >> build.sh
#!/usr/bin/env bash

mkdir build; cd build

cmake ..
cmake --build .

cd ..
EOT
			chmod +x build.sh
		fi
	elif [ $project == "typescript" ] || [ $project == "ts" ]; then
		info "Setting up TypeScript project..."

		dir=""
		while true; do
			askFunction "please provide a project name"
			dir="$temp_answer"
			if [ -d "$dir" ]; then
				error "This directory does already exists."
			else
				break
			fi
		done

		mkdir "$dir"; cd "$dir"

		yarn=0
		if [ command -v "yarn" &> /dev/null ]; then
			if $(askFunction "we detected yarnpkg is installed on your system. should we use it as dependency manager?"); then
				yarn=1
			fi
		fi

		nodejs_exec $yarn "init -y"
		nodejs_exec $yarn "install -D typescript @types/node"

		mkdir src; cd src
		/usr/bin/cat <<EOT >> "main.ts"
console.log('Hello world!');
EOT
		cd ..

		# you can optionally disable this line because it will only fetch my TypeScript configuration file from GitHub
		wget "https://raw.githubusercontent.com/avolgha/angular-app-creator/dev/tsconfig.json" &> /dev/null

		wget "https://gitlab.com/avolgha/sh-scripts/-/raw/dev/more/ts_scripts.js" &> /dev/null
		node "ts_scripts.js"; rm "ts_scripts.js"
	elif [ $project == "javascript" ] || [ $project == "js" ]; then
		info "Setting up JavaScript project..."

		dir=""
		while true; do
			askFunction "please provide a project name"
			dir="$temp_answer"
			if [ -d "$dir"  ]; then
				error "This directory does not exists."
			else
				break
			fi
		done

		mkdir "$dir"; cd "$dir"

		yarn=0
		if [ command -v "yarn" &> /dev/null  ]; then
			if $(askFunction "we detected yarnpkg is installed on your system. should we use it as dependency manager?"); then
				yarn=1
			fi
		fi

		nodejs_exec $yarn "init -y"

		if $(askFunction "should be create a basic project structure for you?"); then
			wget "https://gitlab.com/avolgha/sh-scripts/-/raw/dev/more/js_setup.js" &> /dev/null
			node "js_setup.js"; rm "js_setup.js"
		fi

		if $(askFunction "should we initialize an eslint project for you?"); then
			error "not implemented yet"
		fi
	else
		fatal "don't know provided template"
	fi

	info "Finished :)"
}

add() {
	askFunction "What should be added to the project"
	file="$temp_answer"
	askFunction "Where should be the new file stored"
	store="$temp_answer"

	url="https://gitlab.com/avolgha/sh-scripts/-/raw/dev/extra/$file"
	wget "$url" &> /dev/null
	mv "$file" "$store"

	info "Finished :)"
}

parse_arguments() {
	update=0
	upgrade=0
	generate=0
	add=0
	for arg in "$@"; do
		case "$arg" in
		"--update")
			if [ $update -eq 1 ]; then
				error "cannot set argument '$arg' twice"
			else
				update=1
			fi ;;
		"--upgrade" | "-u")
			if [ $upgrade -eq 1 ]; then
				error "cannot set argument '$arg' twice"
			else
				upgrade=1
			fi ;;
		"--info" | "-i")
			neofetch
			exit 0 ;;
		"--generate" | "-g")
			if [ $generate -eq 1 ]; then
				error "cannot set argument '$arg' twice"
			else
				generate=1
			fi ;;
		"--add" | "-a")
			if [ $add -eq 1 ]; then
				error "cannot set argument '$arg' twice"
			else
				add=1
			fi ;;
		"--list-generates")
			info "cpp, ts, js"
			exit 0 ;;
		*)
			error "unknown argument: '$arg'" ;;
		esac
	done

	if [ $update -eq 1 ]; then
		update
		info "successfull system repository update"
	elif [ $upgrade -eq 1 ]; then
		upgrade
		info "successfull system upgrade"

		if $(acceptFunction "do you want to do a system reboot?"); then
			reboot
		fi
	elif [ $generate -eq 1 ]; then
		generate
	elif [ $add -eq 1 ]; then
		add
	else
		echo "${color_error}fatal:${color_white} please provide any of these arguments:"
		echo "${color_error}fatal:${color_white}        '--update'"
		echo "${color_error}fatal:${color_white} '-u' | '--upgrade'"
		echo "${color_error}fatal:${color_white} '-i' | '--info'"
		echo "${color_error}fatal:${color_white} '-g' | '--generate'"
		echo "${color_error}fatal:${color_white} '-a' | '--add'"
		echo "${color_error}fatal:${color_white}        '--list-generates'"
		exit 1
	fi
}

parse_arguments $@ && exit 0
