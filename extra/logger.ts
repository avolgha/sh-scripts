interface Prefix {
  text: string;
  color: string;
}

export class Logger {
  private prefix: Prefix | undefined;

  constructor(prefix?: Prefix) {
    if (prefix) {
      this.prefix = prefix;
    } else {
      this.prefix = undefined;
    }
  }

  /**
   * This message should not be used in any case except you know what you want
   *
   * @param message The message you want to print
   * @param prefix A prefix for the whole message
   * @param stream A stream you want to print to. Normally `stdout`
   */
  print(
    message: string,
    prefix: string,
    stream: NodeJS.WriteStream = process.stdout
  ) {
    if (this.prefix) {
      stream.write(
        `${prefix}\x1b[0m ${`${this.prefix.color} ${this.prefix.text} `}\x1b[0m \x1b[0;37m${message}\n`
      );
    } else {
      stream.write(`${prefix}\x1b[0m \x1b[0;37m${message}\n`);
    }
  }

  /**
   * Print some success or information to `stdout`
   *
   * @param message The message you want to print
   */
  info(message: string) {
    this.print(message, "\x1b[42m INFO ");
  }

  /**
   * Print some debug to `stdout`
   *
   * @param message The message you want to print
   */
  debug(message: string) {
    this.print(message, "\x1b[46m DEBUG ");
  }

  /**
   * Print some warning to `stdout`
   *
   * @param message The message you want to print
   */
  warn(message: string) {
    this.print(message, "\x1b[43m WARN ");
  }

  /**
   * Print some errors to `stderr`
   *
   * @param message The message you want to print
   */
  error(message: string) {
    this.print(message, "\x1b[41m ERROR ", process.stderr);
  }
}

